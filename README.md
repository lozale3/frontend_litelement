# PRACTITIONER FRONT-END

## Inicializar un proyecto
1.- Se crea la carpeta app dentro del proyecto de Bitbucket (git clone https://lozale3@bitbucket.org/lozale3/frontend_litelement.git)

En cmd (en git bash no es compatible la selección de opciones del menú)

```console
npm init @open-wc
	Scaffold a new project
	Application
	(vacío)
	No
	base-litelement
	Yes
	Yes, with npm
```

para levantar el navegador con el index `npm run start`

## Hello world

Si se añade tres veces la etiqueta <hola-mundo> en el index, al ispeccionar la página web se puede ver como por cada etiqueta está creando un shadow-root y dentro se encontrará el div con el texto "Hola mundo!"

![imagen1](/imagesReadme/imagen1.JPG)

## Ficha persona (constructores), funciones y eventos

Asociar el valor de una propiedad a HTML:

Se crea el constructor para inicializar los atributos que se quiere que tenga el objeto de la clase, por ejemplo:

```javascript
    constructor(){
        super();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 12;
        this.photo = {
            src: "src/images/icon1.png",
            alt: "Foto persona"
        }

    }
```

el codigo para relacionar los valores del html es indicandola siguiente propiedad:
 value="${this.name}"

 En el caso de las imágenes que es un objeto, se accederá a sus propiedades:

```html
 <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"/>
```

 Además, se puede reutilizar el constructor indicándole el valor de los parámetros desde el index.html

```html
 <ficha-persona name="Otra persona" yearsInCompany="2" 
  photo='{"src":"src/images/icon1.png", "alt": "Otra persona"}'></ficha-persona>
```

Para suscribirnos al aviso del evento: @change y una función manejadora. Por ejemplo

`@change="${this.updateName}"`

que cuando el usurio escriba algo en el input de texto nosotros vamos a cambiar
el valor de la propiedad

```html
<input type="text" id="fname" name="fname" value="${this.name}" @change="${this.updateName}"></input>
```

de esta manera se enlaza desde el html a la propiedad, pero no a la inversa. Por eso si se cambia algo en el formulario no va a saltar un evento. Para hacer esto, se creará la función updateName

```javascript
    updateName(e){
        console.log("updateName")
        this.name = e.target.value
	}
```

al clickar fuera de la casilla del formulario tras añadir algo al campo, saltará el change que indicamos en el input del campo

si se cambia change por input, ya no es necesario salir del textbox, pero se va modificando en todos los registros según se escribe:

```html
<input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
```

Crear propiedad calculada (la propiedad se va a calcular en base al valor de otra propiedad)


## Component in component

Para declarar un componente dentro de otro, por ejemplo, incluir persona-header dentro de persona-app hay que añadir el import del componente header en el app y la etiqueta de creación en el render

Fichero `persona-app.js
```javascript
import '../persona-header/persona-header.js';
```

```javascript
    render(){
        return html`
            <h1>
                <persona-header></persona-header>
            </h1>
        `;
    }
```

Declaración de una propiedad que se completará con la salida del componente persona-ficha-listado para obtener ese listado de personas en persona-main

Igual que en groovy pero se le debe indicar la parte del html además de las etiquetas main para la interacción con el navegador.

```javascript
render(){
        return html`
            <h2>Main</h2>
            <main>
            ${this.people.map{
                person => html`<persona-ficha-listado></persona-ficha-listado>`
            }}
            </main>
            
        `;
    }
```

```javascript
	`person => html`<persona-ficha-listado fname=>`
``` 
--> fname hace referencia a la propiedad `fname` de `persona-ficha-listado.js`

Al hacer inyecciones con objetos, se encuentra esta casuística
`.photo="${person.photo}">`

sin el punto, está convirtiendo el valor de la propiedad a un String, en nombre y años en la compañía lo convierte sin problema, pero en este caso, como es un objeto no lo puede convertir y pasa un object. Al pasar a `persona-ficha-listado.js` no puede montar el objeto ni acceder a las propiedades del mismo. Al indicar el punto, lo que hacemos es acceder al objeto (tipo complejo) como una propiedad. En el html no aparece como atributo pero sí que existe dentro del código que se está creando en persona-ficha-listado, por lo que en el img del render lo puede recuperar.

## Estilos

Se trabajará con el framework bootstrap
https://getbootstrap.com/docs/4.5/getting-started/download/

```javascript
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
```

Al definir estilos, hay una división por columnas, de tal manera que si le indicamos la siguiente configuración:

```javascript
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h3>Test bootstrap</h3>
            <div class = "row greybg">
                <div class="col redbg">Col 1</div>
                <div class="col greenbg">Col 2</div>
                <div class="col bluebg">Col 3</div>
        `;
    }
```

se verá de la siguiente manera:

![imagen2](/imagesReadme/imagen2.JPG)

pero si, en cambio, marcamos a cuántas columnas queremos que se apliquen los colores, la página cambiará a la siguiente forma:

```css
    <div class="col-2 redbg">Col 1</div>
    <div class="col-3 greenbg">Col 2</div>
    <div class="col-4 bluebg">Col 3</div>
```

![imagen3](/imagesReadme/imagen3.JPG)

Además, bootstrap genera estilos responsive, por lo que pueden adaptarse a cualquier tipo de tamaño de ventana.

```css
    <div class="col-2 col-sm-6 redbg">Col 1</div>
    <div class="col-3 col-sm-1 greenbg">Col 2</div>
    <div class="col-4 col-sm-1 bluebg">Col 3</div>
```
![imagen4](/imagesReadme/imagen4.JPG)

Por debajo, lo que está ejecutando son mediaqueries.

Para desplazar las columnas se pueden incluir parámetros offset (el máximo es 12)

```css
    <div class="col-2 col-sm-6 offset-1 redbg">Col 1</div>
    <div class="col-3 col-sm-1 offset-2 greenbg">Col 2</div>
    <div class="col-4 col-sm-1 bluebg">Col 3</div>
```
![imagen5](/imagesReadme/imagen5.JPG)

```css
<div class="row" row-cols-1 row-cols-sm-4>
```
row-cols-1 para dispositivos más pequeños y que a partir de sm (móviles) ponga 4


Para que no aparezcan los recuadros de las listas:
`list-group-flush`

## Agregar personas

Para la siguiente práctica, se creará un botón que lanzará un evento, ese evento se recogerá desde el siguiente nivel o siguiente capa, que modificará el contenido de otro componente que dispondrá de un evento receptor para su gestión. Los eventos se propagan hacia arriba, esto quiere decir, que si disponemos de dos componentes al mismo nivel y no existe un componente gestor o balanceador en un nivel superior, ese evento se perderá porque no habrá nadie que lo pueda recuperar y pivotarlo hacia el otro componente.


Eliminar elemento de un array:

El botón está definido en persona-ficha-listado, pero, persona-app contiene persona-main y persona-main contiene persona-fecha-listado. Por lo que al pulsar el botón de persona-ficha-listado habría que llegar al array de persona-main

DATABINDING
Hay dos formas de recuperar y enlazar las propiedades de un formulario, por un lado, a través del ID (como en selenium)

```javascript
this.shadowRoot.getElementById("receiver").course = e.detail.course
this.shadowRoot.getElementById("receiver").year = e.detail.year
```

y la otra es capturando las propiedades en una función manejadora, accediendo el valor de la misma e igualando los atributos de nuestra clase

```javascript
updateName(e){
    console.log("updateName")
    console.log("Actualizando la propiedad name con el valor " + e.target.value)

    this.person.name = e.target.value
}

input type="text" @input="${this.updateName}
```