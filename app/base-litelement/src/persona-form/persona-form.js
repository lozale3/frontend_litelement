import {LitElement, html} from 'lit-element';

class PersonaForm extends LitElement{
    
    static get properties(){
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }

    constructor(){
        super()

        /*this.person = {}
        this.person.name = ""
        this.person.profile = ""
        this.yearsInCompany = ""*/
        this.resetFormData()


    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" id="personFormName" class="form-control" placeholder="Nombre Completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" type="text" class="form-control" placeholder="Años en la empresa" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName(e){
        console.log("updateName")
        console.log("Actualizando la propiedad name con el valor " + e.target.value)

        this.person.name = e.target.value
    }

    updateProfile(e){
        console.log("updateProfile")
        console.log("Actualizando la propiedad profile con el valor " + e.target.value)

        this.person.profile = e.target.value
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany")
        console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value)

        this.person.yearsInCompany = e.target.value
    }

    /*cuando un button está dentro de un form, el comportamiento standard es hacer un summit,
    con la función preventDefault esto se bloquea*/

    goBack(e){
        console.log("goBack")
        e.preventDefault()
        this.resetFormData()
        this.dispatchEvent(new CustomEvent("persona-form-close", {}))
    }

    resetFormData(){
        console.log("resetFormData")
        this.person = {}
        this.person.name = ""
        this.person.profile = ""
        this.person.yearsInCompany = ""

        this.editingPerson = false
    }

    storePerson(e){
        console.log("storePerson")
        e.preventDefault()

        this.person.photo = {
            "src": "src/images/icon1.png",
            "alt": "Persona"
        }

        console.log("La propiedad name vale " + this.person.name)
        console.log("La propiedad profile vale " + this.person.profile)
        console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany)

        /*parámetros CustomEvent: nombre del evento y object*/
        this.dispatchEvent(new CustomEvent("persona-form-store", {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }))
        
    }
}

/* Se crea la etiqueta "hola-mundo" */
customElements.define('persona-form', PersonaForm)