import {LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement{

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        /* se crea la variable de tipo array (que contendrá objetos),
        en el constructor para el listado de personas */
        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 7,
                photo: {
                    "src": "src/images/icon1.png",
                    "alt": "Ellen Ripley"
                },
                profile: "Lorem ipsum dolor sit amet."
            },
            {
                name: "Bruce Banner",
                yearsInCompany: 3,
                photo: {
                    "src": "src/images/icon1.png",
                    "alt": "Bruce Banner"
                },
                profile: "Lorem ipsum."
            },
            {
                name: "Éowyn",
                yearsInCompany: 13,
                photo: {
                    "src": "src/images/icon1.png",
                    "alt": "Éowyn"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                name: "Turanga Leela",
                yearsInCompany: 13,
                photo: {
                    "src": "src/images/icon1.png",
                    "alt": "Turanga Leela"
                },
                profile: "Lorem ipsum dolor sit amet."
            },
            {
                name: "Tyrion Lannister",
                yearsInCompany: 13,
                photo: {
                    "src": "src/images/icon1.png",
                    "alt": "Tyrion Lannister"
                },
                profile: "Lorem"
            }
        ];

        this.showPersonForm = false;
    }

    updated(changedProperties){
        console.log("updated")
        /*changedProperties estructura de tipo map*/
        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main")

            if(this.showPersonForm === true){
                this.showPersonFormData()
            }else{
                this.showPersonList()
            }
        }

        //se añade función para el contador de personas
        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main")
            this.dispatchEvent(
                new CustomEvent("updated-people",{
                    detail: {
                        people: this.people
                    }
                })
            );
        }
    }

    showPersonList(){
        console.log("showPersonList")
        console.log("Mostrando el listado de personas")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none")
        this.shadowRoot.getElementById("personForm").classList.add("d-none")
    }

    showPersonFormData(){
        console.log("showPersonFormData")
        console.log("Mostrando el formulario de persona")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none")
        this.shadowRoot.getElementById("personForm").classList.remove("d-none")
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`
                            <persona-ficha-listado 
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                                fname="${person.name}" 
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                .photo="${person.photo}">
                            </persona-ficha-listado>
                        `
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form
                    @persona-form-store="${this.personFormStore}" 
                    @persona-form-close="${this.personFormClose}" 
                    id="personForm">
                </persona-form>
            </div>
        `;
    }

    /*función receptora del evento @delete-person
    definido en la etiqueta persona-ficha-listado de la función render*/
    deletePerson(e){
        console.log("deletePerson en persona main")

        console.log("Me piden borrar la persona de nombre: " + e.detail.name)
        console.log("Personas antes de borrar: ")
        console.log(this.people)

        //se borra el elemento del array this.people
        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
        /* esta función pide de forma explícita que se actualice
        this.requestUpdate()*/

        console.log("Personas después de borrar: ")
        console.log(this.people)
    }

    infoPerson(e){
        console.log("infoPerson en persona-main")
        console.log("Se ha pedido información de la persona " + e.detail.name)

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )

        //console.log(chosenPerson)

        this.shadowRoot.getElementById("personForm").person = chosenPerson[0]
        this.shadowRoot.getElementById("personForm").editingPerson = true
        this.showPersonForm = true;
    }

    personFormStore(e){
        console.log("personFormStore")
        console.log("Se va a almacenar una persona")

        //console.log(e.detail.person)

        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name)

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person
            )

            //creamos un índice, people, como array, tiene un findIndex. El resultado es la posición del array en la 
            //que se encuentra el elemento. Si no lo localiza, devuelve -1
            // let indexOfPerson = 
            //     this.people.findIndex(
            //         person => person.name === e.detail.person.name
            //     )
            // if (indexOfPerson >= 0){
            //     console.log("Persona encontrada")
            //     this.people[indexOfPerson] = e.detail.person
            // }
        }else{
            console.log("Se va a almacenar una persona nueva de nombre " + e.detail.person.name)
            //this.people.push(e.detail.person)
            //se añade para la función del contador. Los tres puntos es igual al array entero
            // this.people es un array que contiene en la posición 0 otro array, al poner los puntos es como un range
            this.people = [...this.people, e.detail.person]
            console.log("Persona almacenada")
        }
        
        console.log("Proceso terminado")
        this.showPersonForm = false
    }

    personFormClose(e){
        console.log("personFormClose")
        console.log("Se ha cerrado el formulario de la persona")

        /*se podría llamar a this.showPersonList() pero, si en algún momento, se añaden, por ejemplo, más
        funciones, habría que invocarlas a todas. Sin embargo, si se asigna false a la propiedad showPersonList
        que es la que desencadena el flujo, no importaría añadir más pasos en dicho flujo*/
        this.showPersonForm = false
    }

}

customElements.define('persona-main', PersonaMain)